# ShowMeDollar

Simple application for show current ruble to dollar rate and provide admin area for manual settings.

For starting work with this application:

- Clone repo
- Build docker-compose
```
docker-compose build
```
- Then up containers
```
docker-compose up
```

And open browser http://localhost:3000

## Admin area
Open browser http://localhost:3000/admin and you can see simple form for set you own rate and time, until which it will be shown. You cannot set a rate while the old manual rate setting is active.

## Test

Just install dependencies ```bundle install``` and run ```rspec spec```
