# frozen_string_literal: true

class CurrencyCacheQuery
  attr_reader :cache_name

  def self.call(cache_name: nil)
    new(cache_name: cache_name).call
  end

  def initialize(cache_name: nil)
    @cache_name = cache_name
  end

  def call
    cached_value = cache_name.present? ? Redis.new.get(cache_name) : default_caches
    return if cached_value.blank?

    cache, timestamp = cached_value.split('/')
    {
      value: cache,
      time: Time.zone.at(timestamp.to_i).strftime('%d/%m/%Y, %H:%M:%S')
    }
  end

  private

  def default_caches
    Redis.new.get('manual_currency') || Redis.new.get('currency_auto')
  end
end
