# frozen_string_literal: true

require 'net/http'

class CurrencyQuery
  CURRENCY_URI = URI('https://api.exchangeratesapi.io/latest?base=USD&symbols=RUB')

  def self.call
    new.call
  end

  def call
    {
      value: new_value,
      time: Time.current
    }.tap { |hash| save_value_to_cache(hash) }
  end

  private

  def new_value
    response = JSON.parse(Net::HTTP.get(CURRENCY_URI), symbolize_names: true)
    return 'Bad response' if response[:rates].blank?

    response[:rates][:RUB].round(2)
  end

  def save_value_to_cache(hash)
    redis.setex('currency_auto', 60, "#{hash[:value]}/#{hash[:time].to_i}")
  end

  def redis
    @redis ||= Redis.new
  end
end
