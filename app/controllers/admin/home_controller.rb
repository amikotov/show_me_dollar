# frozen_string_literal: true

module Admin
  class HomeController < ApplicationController
    def index
      last_manual_currency = CurrencyCacheQuery.call(cache_name: 'last_manual_currency')
      @currency, @time = last_manual_currency.values if last_manual_currency.present?
    end

    def create
      SetCurrencyManual.call(value: currency_params[:value], time: currency_params[:time])
      redirect_to admin_root_path
    end

    private

    def currency_params
      params.permit(:value, :time)
    end
  end
end
