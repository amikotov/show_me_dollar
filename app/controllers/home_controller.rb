# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    @currency_data = CurrencyCacheQuery.call
  end
end
