# frozen_string_literal: true

module Admin
  module HomeHelper
    def manual_currency_already_on?
      return if @time.blank?

      Time.zone.parse(@time) > Time.current
    end

    def default_time
      return if @time.blank?

      Time.zone.parse(@time).strftime('%Y-%m-%dT%H:%M')
    end

    def minimum_acceptable_time
      1.minute.from_now.strftime('%Y-%m-%dT%H:%M')
    end
  end
end
