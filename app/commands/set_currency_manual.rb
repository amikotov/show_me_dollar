# frozen_string_literal: true

class SetCurrencyManual
  attr_reader :value, :time

  def self.call(*args)
    new(*args).call
  end

  def initialize(value:, time:)
    @value = value
    @time = Time.zone.parse(time).to_i
  end

  def call
    set_cached_value
    store_last_value
    broadcast_to_users
  end

  private

  def broadcast_to_users
    ActionCable.server.broadcast('currency', currency: value, time: current_timestamp)
  end

  def set_cached_value
    expire_time = time - current_timestamp
    redis.setex('manual_currency', expire_time, "#{value}/#{current_timestamp}")
  end

  def store_last_value
    redis.set('last_manual_currency', "#{value}/#{time}")
  end

  def current_timestamp
    @current_timestamp ||= Time.current.to_i
  end

  def redis
    @redis ||= Redis.new
  end
end
