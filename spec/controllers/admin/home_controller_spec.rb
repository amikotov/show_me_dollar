# frozen_string_literal: true

RSpec.describe Admin::HomeController do
  render_views

  describe 'GET #index' do
    subject(:do_request) { get :index }

    before { allow(CurrencyCacheQuery).to receive(:call).and_return({ value: 20, time: '26/11/2018, 14:41:00'} ) }

    it 'expects to be success' do
      do_request

      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    subject(:do_request) { post :create, params: params }

    let(:params) { { value: 5, time: 5.minutes.from_now } }

    before { allow(SetCurrencyManual).to receive(:call) }

    it 'expects to set currency manual' do
      do_request

      expect(response).to redirect_to(admin_root_path)
      expect(SetCurrencyManual).to have_received(:call)
    end
  end
end
