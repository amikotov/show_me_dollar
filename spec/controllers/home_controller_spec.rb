# frozen_string_literal: true

RSpec.describe HomeController do
  describe 'GET #index' do
    subject(:do_request) { get :index }

    before { allow(CurrencyCacheQuery).to receive(:call).and_return({ value: 20, time: 1543226460} ) }

    it 'expects to be success' do
      do_request

      expect(response).to be_successful
      expect(CurrencyCacheQuery).to have_received(:call)
    end
  end
end
