# frozen_string_literal: true

RSpec.describe SetCurrencyManual do
  subject(:set_currency) { described_class.call(value: data[:value], time: data[:time].to_s) }

  let(:data) { { value: 5, time: 5.minutes.from_now } }

  it 'sets currency correctly and send updated data to users' do
    expect { set_currency }.to change { Redis.new.get('manual_currency') }.from(nil)
  end
end
