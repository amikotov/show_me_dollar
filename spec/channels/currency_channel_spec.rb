# frozen_string_literal: true

RSpec.describe CurrencyChannel do
  before { stub_connection }

  it 'subscribes corredtly' do
    subscribe

    expect(subscription).to be_confirmed
  end
end
