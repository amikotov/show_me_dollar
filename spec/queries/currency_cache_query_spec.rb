# frozen_string_literal: true

RSpec.describe CurrencyCacheQuery do
  subject(:get_currency) { described_class.call(cache_name: cache_name) }

  context 'when does not provide custom name of cache' do
    let(:cache_name) { nil }

    before { Redis.new.set('manual_currency', '5/1543227068') }

    it 'gets from default names' do
      expect(get_currency).to eq({ value: '5', time: Time.zone.at(1543227068).strftime('%d/%m/%Y, %H:%M:%S') })
    end
  end
end
