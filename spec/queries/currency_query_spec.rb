# frozen_string_literal: true

RSpec.describe CurrencyQuery do
  subject(:result) { described_class.call }

  before { stub_request(:get, described_class::CURRENCY_URI).to_return(body: { rates: { RUB: 5 } }.to_json) }

  it 'gest result correctly' do
    expect(result[:value]).to eq(5)
  end
end
