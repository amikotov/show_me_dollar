FROM ruby:2.5
RUN ln -fs /usr/share/zoneinfo/Europe/Moscow /etc/localtime && dpkg-reconfigure -f noninteractive tzdata
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /show_me_dollar
WORKDIR /show_me_dollar
COPY Gemfile /show_me_dollar/Gemfile
COPY Gemfile.lock /show_me_dollar/Gemfile.lock
RUN bundle install
COPY . /show_me_dollar
