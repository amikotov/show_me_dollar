Rails.application.routes.draw do
  namespace :admin do
    root 'home#index'
    post '/', to: 'home#create'
  end

  root 'home#index'
end
