# frozen_string_literal: true

if Rails.env != 'test'
  Thread.new do
    loop do
      manual_value_time = Redis.new.ttl('manual_currency')
      if manual_value_time.positive?
        sleep manual_value_time
      else
        new_currency = CurrencyQuery.call
        ActionCable.server.broadcast('currency', currency: new_currency[:value], time: new_currency[:time].to_i)
        sleep 60
      end
    end
  end
end
